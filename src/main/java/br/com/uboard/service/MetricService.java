package br.com.uboard.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.uboard.model.DeveloperDTO;
import br.com.uboard.model.IssueDTO;
import br.com.uboard.model.MemberDTO;
import br.com.uboard.model.MetricDTO;
import br.com.uboard.model.MilestoneDTO;
import br.com.uboard.model.UserDTO;
import br.com.uboard.model.UserMilestonesDTO;
import br.com.uboard.utils.MathUtils;

@Service
public class MetricService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MetricService.class);

	public List<MetricDTO> generateMetrics(List<UserMilestonesDTO> userMilestones) {
		try {
			List<MetricDTO> metricsDTO = new ArrayList<>();

			if (userMilestones != null && !userMilestones.isEmpty()) {
				for (UserMilestonesDTO userMilestonesDTO : userMilestones) {
					if (userMilestonesDTO.getMilestones() != null) {
						for (MilestoneDTO milestoneDTO : userMilestonesDTO.getMilestones()) {
							MetricDTO metricDTO = new MetricDTO();
							metricDTO.setMilestone(milestoneDTO.getTitle());
							metricDTO.setStartDate(milestoneDTO.getStartDate());
							metricDTO.setDueDate(milestoneDTO.getDueDate());

							if (milestoneDTO.getIssues() != null) {
								metricDTO.setTotalIssues(milestoneDTO.getIssues().size());
								metricDTO.setOpenIssuesCount(this.getOpenIssuesCount(milestoneDTO.getIssues()));
								metricDTO.setClosedIssuesCount(this.getClosedIssuesCount(milestoneDTO.getIssues()));
								metricDTO.setReopenedIssuesCount(this.getReopenedIssuesCount(milestoneDTO.getIssues()));
							} else {
								metricDTO.setTotalIssues(0);
								metricDTO.setOpenIssuesCount(0);
								metricDTO.setClosedIssuesCount(0);
								metricDTO.setReopenedIssuesCount(0);
							}

							if (metricDTO.getTotalIssues() == 0) {
								metricDTO.setConclusionPercent(0.0);
								metricDTO.setDeficit(0.0);
							} else {
								metricDTO.setConclusionPercent(this.getConclusionPercent(
										metricDTO.getClosedIssuesCount(), metricDTO.getTotalIssues()));
								metricDTO.setDeficit(this.getDeficitPercent(metricDTO.getOpenIssuesCount(),
										metricDTO.getTotalIssues()));
							}

							metricDTO.setOwner(new UserDTO(userMilestonesDTO.getUserUUID()));
							metricDTO.setDevelopers(this.getDevelopersByMilestone(milestoneDTO));
							metricDTO.setIssues(
									milestoneDTO.getIssues() != null ? milestoneDTO.getIssues() : new ArrayList<>());
							metricsDTO.add(metricDTO);
						}
					}
				}
			}
			return metricsDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return new ArrayList<>();
		}
	}

	private List<DeveloperDTO> getDevelopersByMilestone(MilestoneDTO milestoneDTO) {
		List<DeveloperDTO> developers = new ArrayList<>();

		if (milestoneDTO.getMembers() != null) {
			for (MemberDTO memberDTO : milestoneDTO.getMembers()) {

				int issuesDevelopedCount = 0;
				boolean isIdle = false;
				DeveloperDTO developerDTO = new DeveloperDTO();
				developerDTO.setName(memberDTO.getName());
				developerDTO.setConclusionPercent(0.0);

				if (milestoneDTO.getIssues() != null) {
					for (IssueDTO issueDTO : milestoneDTO.getIssues()) {
						Optional<Long> optionalAssignee = issueDTO.getAssignees().stream()
								.filter(assigneeID -> assigneeID != null && assigneeID.equals(memberDTO.getId()))
								.findAny();

						if (optionalAssignee.isPresent()) {
							if (issueDTO.getState().equals("CLOSED")) {
								issuesDevelopedCount++;
								isIdle = true;
							} else {
								isIdle = false;
							}
						}
					}
				}

				developerDTO.setCompletedIssues(issuesDevelopedCount);
				if (milestoneDTO.getIssues() != null && milestoneDTO.getIssues().size() > 0) {
					Double conclusionPercent = (Double.valueOf(developerDTO.getCompletedIssues())
							/ Double.valueOf(milestoneDTO.getIssues().size())) * 100.0;
					developerDTO.setConclusionPercent(MathUtils.round(conclusionPercent, 2));
				}

				developerDTO.setIsIdle(isIdle);
				developers.add(developerDTO);
			}
		}

		return developers;
	}

	private Integer getOpenIssuesCount(List<IssueDTO> issues) {
		if (issues == null) {
			return 0;
		}
		return issues.stream().filter(issue -> issue.getState().equalsIgnoreCase("OPENED")).collect(Collectors.toList())
				.size();
	}

	private Integer getClosedIssuesCount(List<IssueDTO> issues) {
		if (issues == null) {
			return 0;
		}
		return issues.stream().filter(issue -> issue.getState().equalsIgnoreCase("CLOSED")).collect(Collectors.toList())
				.size();
	}

	private Integer getReopenedIssuesCount(List<IssueDTO> issues) {
		if (issues == null) {
			return 0;
		}
		return issues.stream().filter(issue -> issue.getState().equalsIgnoreCase("REOPENED"))
				.collect(Collectors.toList()).size();
	}

	private Double getConclusionPercent(Integer closedIssuesCount, Integer totalIssues) {
		Double result = (Double.valueOf(closedIssuesCount) / Double.valueOf(totalIssues)) * 100.0;
		return MathUtils.round(result, 2);
	}

	private Double getDeficitPercent(Integer openIssuesCount, Integer totalIssues) {
		Double result = (Double.valueOf(openIssuesCount) / Double.valueOf(totalIssues)) * 100.0;
		return MathUtils.round(result, 2);
	}
}
