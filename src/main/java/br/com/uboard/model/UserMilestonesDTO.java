package br.com.uboard.model;

import java.io.Serializable;
import java.util.Set;

public class UserMilestonesDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String userUUID;
	private Set<MilestoneDTO> milestones;

	public String getUserUUID() {
		return userUUID;
	}

	public void setUserUUID(String userUUID) {
		this.userUUID = userUUID;
	}

	public Set<MilestoneDTO> getMilestones() {
		return milestones;
	}

	public void setMilestones(Set<MilestoneDTO> milestones) {
		this.milestones = milestones;
	}

}
