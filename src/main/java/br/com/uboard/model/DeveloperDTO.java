package br.com.uboard.model;

import java.io.Serializable;

public class DeveloperDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private Integer completedIssues;
	private Double conclusionPercent;
	private Boolean isIdle;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCompletedIssues() {
		return completedIssues;
	}

	public void setCompletedIssues(Integer completedIssues) {
		this.completedIssues = completedIssues;
	}

	public Double getConclusionPercent() {
		return conclusionPercent;
	}

	public void setConclusionPercent(Double conclusionPercent) {
		this.conclusionPercent = conclusionPercent;
	}

	public Boolean getIsIdle() {
		return isIdle;
	}

	public void setIsIdle(Boolean isIdle) {
		this.isIdle = isIdle;
	}

}
