package br.com.uboard.model;

import java.io.Serializable;

public class UserDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String uboardIdentifier;

	public UserDTO() {

	}

	public UserDTO(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

	public String getUboardIdentifier() {
		return uboardIdentifier;
	}

	public void setUboardIdentifier(String uboardIdentifier) {
		this.uboardIdentifier = uboardIdentifier;
	}

}
