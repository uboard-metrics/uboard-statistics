package br.com.uboard.config.consumer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.uboard.config.producer.Topics;
import br.com.uboard.model.MetricDTO;
import br.com.uboard.model.UserMilestonesDTO;
import br.com.uboard.service.MetricService;

@Component
public class MetricsConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(MetricsConsumer.class);
	private static final String SEND_STATISTICS_TOPIC = "send-statistics-topic";

	private MetricService metricService;

	private KafkaTemplate<String, List<MetricDTO>> kafkaTemplate;

	public MetricsConsumer(MetricService metricService, KafkaTemplate<String, List<MetricDTO>> kafkaTemplate) {
		this.metricService = metricService;
		this.kafkaTemplate = kafkaTemplate;
	}

	@KafkaListener(groupId = "sync-metrics-group", topics = SEND_STATISTICS_TOPIC, containerFactory = "metricsContainerFactory")
	public void receive(@Payload List<UserMilestonesDTO> metricsList) throws Exception {
		try {
			LOGGER.debug("Getting {} metrics to calc", metricsList.size());
			List<MetricDTO> metrics = this.metricService.generateMetrics(metricsList);

			LOGGER.debug("Getting {} calculated metrics to send", metrics.size());
			if (!metrics.isEmpty()) {
				this.kafkaTemplate.send(Topics.SEND_METRICS_TOPIC, metrics);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
