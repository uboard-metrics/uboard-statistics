package br.com.uboard.config.consumer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.uboard.model.UserMilestonesDTO;

@Configuration
public class MetricsConsumerConfiguration {

	@Autowired
	private KafkaProperties properties;

	@Bean
	public ConsumerFactory<String, List<UserMilestonesDTO>> metricsConsumerFactory() {
		Map<String, Object> configs = new HashMap<>();
		configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServers());
		configs.put(ConsumerConfig.GROUP_ID_CONFIG, "sync-metrics-group");
		ObjectMapper objectMapper = new ObjectMapper();
		JavaType type = objectMapper.getTypeFactory().constructParametricType(List.class, UserMilestonesDTO.class);
		return new DefaultKafkaConsumerFactory<>(configs, new StringDeserializer(),
				new JsonDeserializer<List<UserMilestonesDTO>>(type, objectMapper, false));
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, List<UserMilestonesDTO>> metricsContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, List<UserMilestonesDTO>> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(metricsConsumerFactory());
		return factory;
	}
}
